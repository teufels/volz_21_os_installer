Please copy staging_dummy.ini. Then change content.

```
cp staging_dummy.ini staging.ini
```

Please copy sudo_dummy.ini. Then change content.  Do _not_ use "$" in password.

```
cp sudo_dummy.ini sudo.ini
```
