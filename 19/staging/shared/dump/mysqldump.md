```
mysqldump \
-hmysql dev \
-udev \
-pdev \
--ignore-table=dev.sys_log \
--ignore-table=dev.tx_realurl_pathdata \
--ignore-table=dev.tx_realurl_uniqalias \
--ignore-table=dev.tx_realurl_uniqalias_cache_map \
--ignore-table=dev.tx_realurl_urldata \
--single-transaction \
--skip-add-locks \
--no-autocommit > data.sql
```

```
mysqldump \
-hmysql dev \
-udev \
-pdev \
--no-data > definition.sql
```