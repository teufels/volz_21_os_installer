![version 8.11.0](https://img.shields.io/badge/version-9.5.0-green.svg?style=flat-square)
   ![license MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)

```
NAME
    hive_boilerpalte_v3

SYNOPSIS
    Good starting point for a TYPO3 project

DESCRIPTION
    Install TYPO3 in a preconfigured docker machine. Deploy your project via bitbucket piplines.

REQUIREMENTS
    Git
    Docker (for Mac) [https://www.docker.com/]
    Docker Compose [https://docs.docker.com/compose/]
    Docker Sync (on Mac) [http://docker-sync.io/]
    Bash  

INSTALLATION 

    On Bitbucket:

        Create a (private) project and fork 'hive_boilerplate_v3' (https://bitbucket.org/teufels/hive_boilerplate_v3) and 'hive_thm_custom' (https://bitbucket.org/teufels/hive_thm_custom) into the project.

        Name the repos '<customer_abbr>_<yy>_<type>_installer' and '<customer_abbr>_<yy>_<type>_thm'

        Create a branch 'custom' in '<customer_abbr>_<yy>_<type>_installer' from most recent 'release/phpX.Y' branch.
        Create a branch 'custom' in '<customer_abbr>_<yy>_<type>_thm' from most recent 'release/X.Y' branch.
    
    On your computer:

        Creat a project with your preferred IDE.

        Clone the fork of '<customer_abbr>_<yy>_<type>_installer' into your project.

        Edit '.env' and replace placeholders with a unique value. Otherwise docker would override containers from multiple projects with each other.
        
        Edit 'composer.json' and replace placeholder with '<customer_abbr>_<yy>_<type>_thm'.

        Start docker (e.g. via 'sudo systemctl start docker.service' on arch linux)
        
        Rum 'docker-sync start --foreground'          
     
        Run 'sudo make up-on-osx' or 'make up-on-linux' to build the docker containers.
        
            (If Apache not run stop docker 'docker stop $(docker ps -a -q)' and start docker-sync again 'docker-sync clean && docker-sync start --foreground')
        
            Copy your '.ssh' from your local root into the installer with 'cp -rp ~/.ssh .'
            
        Change into the built app container via 'sudo make root-on-osx' or make 'root-on-linux'.
                
            Copy the synced '.ssh' folder in the root of the app container with 'cp -rp .ssh ~/.'

            Optional Run 'chmod -R 600 ~/.ssh/'

            Run 'sh /app/docker_install_typo3.sh'.

            Run 'mysql -hmysql dev -udev -pdev < /app/typo3v9_data.sql'

            Optional run 'chmod -R 777 web && chmod -R 777 var'

        Change into built phpgulp container via 'sudo make gulp-on-osx' or 'make gulp-on-linux'.

            (Run 'gulp --gulpfile=docker_gulpfile.js'. not necessary any more)

        
        ENJOY a ready to use TYPO3 installation :)
        
        
        Optional: Docker-sync
            
            Run sync in your project with 'docker-sync start --foreground'
            
            following not necessary any more:
                - docker-compose.osx.yml:
                Remove the # tag from the following lines and replace the Placeholder:
        
                #- <yymmddhhmm>-<customer_abbr>-sync:/app/:nocopy # will be mounted on /app
        
                #volumes:  
                #  <yymmddhhmm>-<customer_abbr>-sync:
                #    external: true
        
                and comment following line out:
        
                - ./:/app/ #comment out if using line above
        
                -docker-sync.yml:
                Replace the following Placeholder (All three Placeholders should be replaced with the same Timestamp):
        
                <yymmddhhmm>-<customer_abbr>-sync: # tip: add -sync and you keep consistent names as a convention
        
                Run sync in your project with 'docker-sync start --foreground'

LISENCE:
    The MIT License (MIT)

    Copyright (c) 2019
    
    Albert Giss <a.giss@teufels.com>,
    Andreas Hafner <a.hafner@teufels.com>,
    Bastian Holzem <b.holzem@teufels.com>,
    Hendrik Krueger <h.krueger@teufels.com>,
    Marcel Weber <m.weber@teudels.com>,
    Timo Bittner <t.bittner@teufels.com>,
    Yannick Aister <y.aister@teufels.com>,
    teufels GmbH <digital@teufels.com>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
```